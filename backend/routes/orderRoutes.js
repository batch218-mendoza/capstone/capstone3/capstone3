const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController =  require("../controllers/userController.js");
const auth = require("../auth.js");
const Product = require("../models/product.js");
const orderController =  require("../controllers/orderController.js");

router.post("/createorder", auth.verify, (req, res,) =>{

	const newData = {	
		userId: req.body,
		name:req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderController.createOrder(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)	
		})
})

//get all orders
router.get("/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	orderController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController))
})




module.exports = router;