
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");
const User = require("../models/user.js");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});



router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});



router.post("/details", (req, res) => {
	userController.getProfileA({userId : req.body.id}).then(resultFromController => res.send(resultFromController))
});


router.get("/details/:id", (req, res) => {
	userController.getProfileB(req.params.id).then(result => res.send(result))
});


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		
		userId : auth.decode(req.headers.authorization).id,
		
		productId : req.body.productId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

});




module.exports = router;