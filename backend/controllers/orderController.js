const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");



module.exports.createOrder = (reqBody, newData) => {
if(newData.isAdmin == false){

	let newOrder = new Order ({
		userId: reqBody.userId,
		product: [{
			name: reqBody.name,
			quantity: reqBody.quantity
		}]

	})


	return newOrder.save().then((newProduct, error) => {
		if(error){
				return error;
		}
		else{
			return newProduct;
		}
	})
}else{
	let message = Promise.resolve('ADMIN CANNOT CREATE ORDER');
		return message.then((value) => {return value})
	}
}
//get all orders
module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
	return result;
})
}


module.exports.getAllOrders = (isAdmin) => {
	if(isAdmin == true){
		return Order.find({}).then((result, error) => {
			if(error){
				return false;
			}
			return result;
		
		})

		}
		else {
			let message = Promise.resolve('you must be admin to get all orders');
			return message.then((value) => {return value})
	}

}


