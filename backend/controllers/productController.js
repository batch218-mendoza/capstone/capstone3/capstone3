const mongoose = require("mongoose");
const Product = require('../models/product.js');



module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	
		
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
};




/*	module.exports.addProduct = (data) => {
		console.log(data.isAdmin)

		if(data.isAdmin) {
			let newProduct = new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			});

			return newProduct.save().then((newProduct, error) => {
				if(error){
					return error
				}
				return newProduct 
			})
		};*/

		
/*		let message = Promise.resolve('User must be ADMIN to access this.')

		return message.then((value) => {
			return {value}
		})
	};*/



module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
};



module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
			return result;
	})
};


module.exports.getProduct = (productId) => {


	return Product.findById(productId).then(result => {
		return result;
	})
};



/*module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , 
			{			//req.body  
				name: newData.course.name,
				description: newData.product.description,
				price: newData.product.price
			}
		).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
};
*/


/*module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		} 
		return {
			message: "Product archived successfully!"
		}
	})
};
*/

module.exports.checkProductExist = (reqBody) => {

	return Product.find({name: reqBody.name,}).then(result => {
	
		if(result.length > 0){
			return true;
		}
		
		else
		{
			return false;
		}
	})
};

// update product
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , {
			name: newData.product.name,
			description: newData.product.description,
			price: newData.product.price,
		}).then((updatedProduct, error) => {
			if(error){
				return false;
			}
			
			return newData;
		
		})

		}
		else {
			let message = Promise.resolve('User must be ADMIN to update a product');
			return message.then((value) => {return value})
	}

}


module.exports.archive = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , {
			isActive: newData.product.isActive
		}).then((updatedProduct, error) => {
			if(error){
				return false;
			}
			
			return { message: "Product archived successfully"};
		
		})

		}
		else {
			let message = Promise.resolve('User must be ADMIN to archive product');
			return message.then((value) => {return value})
	}

}

