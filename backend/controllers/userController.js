
const User = require("../models/user.js");
const Product = require('../models/product.js');
const Order = require("../models/order.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Checking Email
module.exports.checkEmailExist = (reqBody) => {
	
	return User.find({email: reqBody.email }).then(result => {
		
		if(result.length > 0){
			return true;
		}
		
		else
		{
			return false;
		}
	})
};



module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
	
		password: bcrypt.hashSync(reqBody.password, 10),
	
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
};



module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
		
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				
				return false;
				
			}
		}
	})
};


// A
module.exports.getProfileA = (reqBody) => {
	return User.findById(reqBody.userId).then((details, err) => {
		if(err){
			return false;
		}
		else{
			details.password = "*****";
			return details;
		}
	})
}

// B
module.exports.getProfileB = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
};


// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			return result
		}
	})
};


module.exports.enroll = async (data) => {

	
	let isUserUpdated = await User.findById(data.userId).then(user => {

		
		user.cart.push({productId : data.productId});

		
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	
	let isProductUpdated = await Product.findById(data.productId).then(product => {

		
		product.cart.push({userId : data.userId});

		
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	
	if(isUserUpdated && isProductUpdated){
		return true;
	
	} else {
		return false;
	};

};


